FROM debian:stable-slim AS builder
RUN apt-get update -y && \
	apt-get install -y git cmake make build-essential gcc

WORKDIR /build
COPY . .

RUN cd build/cmake
RUN cmake /build -DCMAKE_BUILD_TYPE=Release 
RUN make
RUN make install

FROM debian:stable-slim AS runtime
COPY --from=builder /usr/local/bin/tidy /usr/local/bin/tidy
ENTRYPOINT [ "/usr/local/bin/tidy" ]
